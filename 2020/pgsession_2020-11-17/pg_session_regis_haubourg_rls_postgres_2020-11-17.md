# Row Level Security Policies

Filtrage par ligne 

appliqué à des applications géospatiales

*Régis Haubourg - Oslandia*


Note:
La fenêtre du speaker peut être ouverte avec la touche 's'
Pour noircir la fenêtre, presser 'b'

## /me

```
Régis Haubourg

Géomaticien passionné
ex-administrateur de données géographiques pour une agence de l'eau 
    >>> "SQL saved my life"

Expert géomaticien Open Source chez Oslandia


QGIS & PostGIS fan


Président de l'OSGeo-FR 

                                                                      ...slowly becoming a nerd 

```


---

## OSLANDIA

PME Française crée en 2009

Haute technologie

Open source100% télétravail


---

## OSLANDIA

15 collaborateurs

100% télétravail


---

<!-- .slide: data-background="images/background_logos_communities.svg" data-background-size="100%" -->

### Communautés Open Source & Libres

QGIS 

PostGIS

Giro3D-iTowns (pointcloud / imagerie / 3D immersive)

python, SQL, C++, javascript

Stack OSGeo



---


### Le besoin métier 

### ---

### Filtrer des lignes

---


##### Gestion de réseau d'eau, cadastre, route

### --- 

Ex: 

Un utilisateur ne doit pouvoir éditer que les données de son territoire. 

Un administrateur d'un syndicat intercommunal doit pouvoir accéder à toutes ses communes, mais pas les autres 

---

![tigre](images/tigre_arrond.png)

---


![tigre](images/tigre_zoom.png)

---

### Des outils Desktop

 - QGIS, le client de référence PostgreSQL - PostGIS 

 - **Accès direct aux couches PG**

---

![couche pg qgis](images/layer_qgis_pg.png)

---



Gestion de droit pilotée par la base de données

<img src="images/qgis_direct_read_postgres.svg" style="width:400px">
 -

---

### Les droits PostgreSQL classiques

- Table 
- Vue 
- Colonne


Nécessaire pour filtrer les droits de base 

Admin / Édition / consultation

INSUFFISANT

---

### Filtrer les lignes en fonction de l'utilisateur courant

Variable dynamique nécessaire

---

![variables](images/current_user_current_session.png)


`session_user`   : utilisateur de la session qui a servi à la connexion


`current_user ` : l'utilisateur courant ou effectif


---

![variables](images/current_user_current_session_changes.png)


---


### La méthode traditionnelle

#### Gestion des droits applicative
  
```
CREATE TABLE user (...) 

CREATE TABLE user_group (...) 

CREATE TABLE group  (...) 

CREATE TABLE group_commune (...) 

```

**L'applicatif filtre les données**

**Mais toujours avec un seul role PG**


---

#### Solution Hacky avec des vues

`current_user` et  `session_user` dans les filtres `WHERE` des vues


---

#### Version simplifiée 

```sql
CREATE VIEW my_user AS

SELECT *

FROM "user"

WHERE

   "user"."user" = current_user; -- or current_session

--

```

---

### Exemple pédagogique

on ne travaille jamais avec des utilisateurs avec droit de connexion

Et on utilise de groupes

---

#### Modèle de données avec territoire / groupes 

```sql
 SELECT * 
 FROM communes 
 WHERE 
   id_commune IN 
   ( SELECT id_commune 
     FROM group_zone gz JOIN user_group ug 
     ON ug.group_id = gz.fk_group_id 

     -- here we filter:
     WHERE gz.user_id = current_session )  

```

> note : on peut éviter l'utilisation d'une table explicite d'utilisateurs avec des requêtes système

---

### LIMITES


- Nécessite de modifier les vues en place (MIGRATION)

- Pas sécure ( security_barrier )

- Pas désactivable

---


### RLS - Row Level Security policies

La belle manière de faire. 


[ Politiques de sécurité niveau ligne](https://docs.postgresql.fr/13/ddl-rowsecurity.html)


---

## La syntaxe 



---

```sql

CREATE TABLE comptes (admin text, societe text, contact_email text);

-- activation des politiques de sécurité sur la table
ALTER TABLE comptes ENABLE ROW LEVEL SECURITY;

-- à ce stade, les utilisateurs ne voient plus les données

-- activation d'une politique

CREATE POLICY compte_admins ON comptes TO admins
    USING (admin = current_user);

```

---

### variations 

```sql 

-- activation de deux politiques combinées 

-- lecture pour tous
CREATE POLICY compte_admins ON comptes TO admins
 FOR SELECT
    USING (true);

-- modification uniquement pour ses propres données 
-- Combinaison OR par défaut

CREATE POLICY user_mod_policy ON users
    -- critère de lecture
    USING (user_name = current_user)
    -- vérification des données à enregistrer
    WITH CHECK (user_name = current_user)  
    ;

```

---

### Démo

---

### Filtrer des tronçons de canalisation - Application QWAT

--- 

![tables](images/tables.png)

---

![users](images/users.png)

---


![qgis01](images/qgis01.png)

---

![qgis02](images/qgis02.png)

---


![qgis03](images/qgis03.png)

---

## Filtrage sur une autre table - Bornes incendies

---

![qgis04](images/qgis04.png)

---

# WTF ????

Compte identique avec ou sans RLS


---

### C'est une vue !

```sql
CREATE OR REPLACE VIEW qwat_od.vw_element_installation
```

![qgis05](images/qgis05.png)

---

![qgis05](images/bartfront.png)

---

# DROITS MIXTES

---

![qgis05](images/users_RO.png)

---

![qgis06](images/qgis06.png)

---

### Filtrage géographique postGIS

<!-- 
```sql
CREATE POLICY district_filter_select ON qwat_od.leak  FOR SELECT  
USING ( true = 
( SELECT st_dwithin(leak.geometry, d.geometry, 10) 
	FROM   qwat_od.district d 
	JOIN qwat_od.user_district ud 
	ON (ud.fk_district = d.id AND ud.privilege = 'RO' AND ud.fk_login = session_user)
  -- filtre de distance 10 mètre à un point
	WHERE st_dwithin(st_setsrid(st_makepoint(515156,157771), 21781), d.geometry, 10)

 ) )  ;
``` -->

![qgis08](images/qgis08.png)




---

# En synthèse

---


* Assez technique à rédiger

* Choix de règles AND (restrictif) / OR (permissif)

* Combinaison avec droits par colonne

---

### KEEP. IT. SIMPLE. STUPID.

### ---

### TESTEZ. TOUJOURS.

---

Ce sont des évaluations `WHERE`. 

Avec impact sur la performance.

#### OPTIMISEZ VOS requêtes et indexes

---

Performances si PG < 10  

(Scan séquentiel avant filtrage RLS)


---

### Et avec des filtres spatiaux ? 


- performance : point dans polygone OK. KNN OK. 

- Grandes géométries - requêtes coûteuses de recouvrement > **DANGER!**

- Erreur de topologie? : **Réponse VIDE et erreurs SILENCIEUSES** !

---

#### Créer un ROLE de base pour chaque utilisateur ?

Pas toujours possible ni souhaitable (appli web) 

Filtrage avec **Variables de session**

```sql

SET rls.ename = 'smith';

--- 

CREATE POLICY emp_rls_policy ON employee FOR all TO public 

USING(ename=current_setting('filtre.ename'));


```

---

### avec les vues 
 
  - il suffit de filtrer les tables sous jacentes! rien à faire sur le vues.

  - La lecture des tables d'une vue exploite l'utilisateur propriétaire de la vue: 
    - ~~current_user~~ > `session_user`
 
  - Si le propriétaire est superuser, les RLS sont bypassées

    - Explicitement avoir des propriétaires NON superuser

  

<!--  interactions des 
> security_barrier nécessaire pour éviter les fuites de données. > moins bonnes performances. 
>  Les vues peuvent permettre d'accéder aux données filtrées via des fonctions simples - risque de sécurité. 
> EXPLAIN permet d'extraire des informations des tables non filtrées 
-->

---

### Références 


Bennie Swart - Octobre 2018 - PostgresConf South Africa 2018 :

https://postgresconf.org/conferences/SouthAfrica2018/program/proposals/row-level-security


https://dba.stackexchange.com/questions/83216/how-to-restrict-access-to-current-user-record-and-its-rights-in-postgres

http://rhaas.blogspot.com/2012/03/security-barrier-views.html

https://stackoverflow.com/questions/33858030/why-isnt-row-level-security-enabled-for-postgres-views

---



## Merci de votre attention 

<!-- .slide: data-background="#111" -->

#### et N'oubliez pas les journées QGIS-FR les 15 et 16 décembre !

[https://twitter.com/JourneesQgis](https://twitter.com/JourneesQgis)

![logo_journéees](images/logo_journees_crop.png)

---