---
type: slide
title: QGeologis
slideOptions:
  transition: slide
  theme: white
---

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/background_title.png" data-background-size="1700px" data-background-color="#fff"-->

## QGeoloGIS

### Visualisation de colonnes stratigraphiques et de séries temporelles
Journées QGIS utilisateurs francophones
Sébastien Peillet - Oslandia

---

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

## Contexte

 * QGIS utilisé par les géologues 
 * Besoin de visualiser leur données dans l'espace en 2D mais aussi la 3ème, voire la 4ème aussi

![](https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/illu1.jpg =200x200)

----

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

Données nécessaire : 
* profondeur (depuis, jusqu'à / depuis + intervalle)
* valeur (stratigraphie, mesures)
<br/>A chacun son usage

![](https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/illu2.jpg =500x180)

---

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

## Présentation

<span style="font-size:16pt;">Dépôt : https://gitlab.com/Oslandia/qgis/QGeoloGIS
Documentation : https://oslandia.gitlab.io/qgis/QGeoloGIS/</span>

* Dans QGIS :

    * une couche de point de référence
    * des couches de données attributaires
    * une configuration QGeoloGIS

----

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

![](https://pad.oslandia.net/uploads/upload_2e54f6c367fb9615c28a4b0ca14b43d2.png =800x)

---

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

## Comment l'utiliser

Jeu de donnée présent dans le dépôt : 
* projet QGIS 

    * Geopackage
    * Postgresql

----

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

### Visualiser

#### Logs stratigraphiques

----

<!-- .slide: data-background-video="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/media/qgeologis_prez1.mp4" data-background-color="#000000" -->

----

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

#### Séries temporelles

----

<!-- .slide: data-background-video="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/media/qgeologis_prez2.mp4" data-background-color="#000000" -->

----

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

### Configurer

----

<!-- .slide: data-background-video="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/media/qgeologis_prez3.mp4" data-background-color="#000000" -->

---

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->


## Feuille de route

- importation des graphiques dans le gestionnaire de mise en page
- ajout des symbologies graduées/catégorisées.
- mise à jour des configuration de colonnes
- sélection de points ordonnés pour réaliser du rendu de coupe

---

<!--.slide:  data-background="https://gitlab.com/Oslandia/documentation/presentations/-/raw/master/2020/journee_qgis_utilisateur/qgeologis/img/oslandia_background_page.png" data-background-size="1600px" data-background-color="#fff"-->

## Remerciement

CEA et Orano

QGIS et OSGeo-fr pour l'organisation

contact@oslandia.com
